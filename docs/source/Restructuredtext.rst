########################
Restructured Text syntax
########################

J'aime bien ce lien `<https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_

==========
 Sections
==========

* # dessus et dessous
* * dessus et dessous
* =
* -
* ^
* "

====================
 Comment souligner?
====================

* ESC
* yy copie la ligne
* p paste la ligne copiée en dessous
* V sélectionne la ligne copiée
* r= remplace la ligne selectionnée par le caractère =

* C-=  On peut changer le niveau. mais il faut que d'autres niveaux existent déjà dans le document.

==============
 Key bindings
==============

* C-= pour mettre les décorations des titres
* On repete C-= pour avoir les différents niveaux.

'voir ici <https://docutils.sourceforge.io/docs/user/emacs.html#checking-situation>'_
